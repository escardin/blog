package ca.thelovelaces.blog;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/post")
public class PostREST {

    PostDAO postDAO = new PostDAO();
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Post> listPosts() {

        return postDAO.getPosts();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Post getPost(@PathParam("id") int id) {
        return postDAO.getPost(id);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Post getPost(Post post) {
        return postDAO.savePost(post);
    }
}
