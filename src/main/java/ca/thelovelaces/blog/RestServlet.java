package ca.thelovelaces.blog;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("api")
public class RestServlet extends ResourceConfig {
    public RestServlet() {
        super(PostREST.class);
        register(JacksonFeature.class);
    }

}
