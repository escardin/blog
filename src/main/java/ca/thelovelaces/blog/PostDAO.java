package ca.thelovelaces.blog;

import ca.thelovelaces.jpa.JPASession;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class PostDAO {
    private final JPASession session = new JPASession();

    List<Post> getPosts() {
        EntityManager manager = session.entityManager();
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<Post> query = criteriaBuilder.createQuery(Post.class);
        Root<Post> selector = query.from(Post.class);
        query.select(selector);
        TypedQuery<Post> tQuery = manager.createQuery(query);
        List<Post> posts = tQuery.getResultList();
        session.closeEntityManger();
        return posts;
    }

    Post getPost(int id) {
        EntityManager manager = session.entityManager();
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<Post> query = criteriaBuilder.createQuery(Post.class);
        Root<Post> selector = query.from(Post.class);
        query.select(selector);
        query.where(criteriaBuilder.equal(selector.get("id"), id));
        TypedQuery<Post> tQuery = manager.createQuery(query);
        Post post = tQuery.getSingleResult();
        session.closeEntityManger();
        return post;
    }

    Post savePost(Post post) {
        EntityManager manager = session.entityManager();
        manager.persist(post);
        manager.getTransaction().commit();
        session.closeEntityManger();
        return post;
    }
}
