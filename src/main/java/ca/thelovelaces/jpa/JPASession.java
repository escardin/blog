package ca.thelovelaces.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by mike on 2015-02-16.
 */
public class JPASession {
    private static final EntityManagerFactory entityManagerFactory = buildEntityManagerFactory();
    private EntityManager entityManager;

    private static EntityManagerFactory buildEntityManagerFactory() {
        try {
            return Persistence.createEntityManagerFactory("ca.thelovelaces.blog");
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static void shutdown() {
        // Close caches and connection pools
        entityManagerFactory.close();
    }

    public EntityManager entityManager() {
        if (null != entityManager)
            return entityManager;
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        return entityManager;
    }

    public void closeEntityManger() {
        entityManager.close();
        entityManager = null;
    }


}
