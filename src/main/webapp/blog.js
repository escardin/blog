/**
 * Created by mike on 2015-02-16.
 */

var blog = angular.module("blogApp", []);
blog.controller('BlogController', function ($scope, $http) {
    "use strict";
    $http.get("api/post").success(function (data) {
        $scope.posts = data;
    }).error(function () {
        $scope.posts = [{"title": "Error getting posts", "body": "There was an error getting data from the server"}];
    });
});
